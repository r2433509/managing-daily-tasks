# Практическая работа №2.

## Описание программы
Допустим, у нас есть приложение для управления ежедневными задачами и расписанием. Мы хотим использовать паттерны Одиночка, Фасад и Итератор для упрощения управления задачами и обеспечения удобства использования в бытовой жизни.

Одиночка (Singleton): В коде используется паттерн Одиночка для класса TaskManager. Он обеспечивает создание и доступ к единственному экземпляру класса TaskManager, который управляет списком задач. Благодаря паттерну Одиночка, мы можем гарантировать, что в приложении будет только один экземпляр TaskManager, и все операции с задачами будут выполняться через этот экземпляр.

Фасад (Facade): Класс TaskManagerFacade выступает в роли фасада. Он предоставляет упрощенный интерфейс для работы с задачами, скрывая детали реализации класса TaskManager. TaskManagerFacade позволяет добавлять новые задачи, завершать задачи и получать списки незавершенных и завершенных задач. Фасад упрощает использование TaskManager, предоставляя более удобные методы для взаимодействия с задачами.

Итератор (Iterator): Классы UncompletedTaskIterator и CompletedTaskIterator реализуют паттерн Итератор. Они представляют итераторы для обхода списка незавершенных и завершенных задач соответственно. Оба класса реализуют интерфейс ITaskIterator, который определяет методы HasNext() и GetNext() для проверки наличия следующего элемента и получения следующего элемента в итераторе. Благодаря паттерну Итератор, мы можем удобно перебирать задачи в списке незавершенных и завершенных задач без необходимости знать детали их реализации.

## Одиночка (Singleton):
Паттерн Одиночка гарантирует, что класс имеет только один экземпляр и предоставляет глобальную точку доступа к этому экземпляру. Он обеспечивает удобный и единообразный доступ к объекту в программе. Это особенно полезно, когда требуется, чтобы один объект был общим ресурсом для других объектов или когда требуется предотвратить создание нескольких экземпляров объекта.

## Фасад (Facade):
Паттерн Фасад предоставляет простой унифицированный интерфейс для взаимодействия с более сложной подсистемой классов. Он скрывает сложность внутренней реализации и предоставляет упрощенный интерфейс, с которым клиенты могут работать. Фасад упрощает взаимодействие с подсистемой и уменьшает зависимости между клиентом и подсистемой.

## Итератор (Iterator):
Паттерн Итератор предоставляет способ последовательного доступа и обхода элементов коллекции без раскрытия деталей ее реализации. Он позволяет получать элементы коллекции последовательно, не зная о внутренней структуре коллекции. Итератор абстрагирует обход элементов, предоставляя единый интерфейс для доступа к элементам коллекции независимо от ее конкретной реализации.




## Uml программы

```plantuml
@startuml

class Task {
    + Name: string
    + IsCompleted: bool
}

class TaskManager {
    - instance: TaskManager
    - tasks: List<Task>
    - TaskManager()
    + Instance(): TaskManager
    + AddTask(task: Task): void
    + RemoveTask(task: Task): void
    + GetTasks(): IEnumerable<Task>
    + GetUncompletedTaskIterator(): ITaskIterator
    + GetCompletedTaskIterator(): ITaskIterator
}

interface ITaskIterator {
    + HasNext(): bool
    + GetNext(): Task
}

class UncompletedTaskIterator {
    - tasks: List<Task>
    - currentIndex: int
    + UncompletedTaskIterator(tasks: List<Task>)
    + HasNext(): bool
    + GetNext(): Task
}

class CompletedTaskIterator {
    - tasks: List<Task>
    - currentIndex: int
    + CompletedTaskIterator(tasks: List<Task>)
    + HasNext(): bool
    + GetNext(): Task
}

class TaskManagerFacade {
    - taskManager: TaskManager
    - uncompletedTasks: List<Task>
    - completedTasks: List<Task>
    + TaskManagerFacade()
    + AddNewTask(taskName: string): void
    + CompleteTask(task: Task): void
    + GetUncompletedTaskIterator(): ITaskIterator
    + GetCompletedTaskIterator(): ITaskIterator
}

class Program {
    + Main(args: string[]): void
}

TaskManager "1" --* "0..*" Task
TaskManager "1" *-- "1" ITaskIterator
TaskManagerFacade "1" -- "1" TaskManager
TaskManagerFacade "1" *-- "1" ITaskIterator
UncompletedTaskIterator "1" --* "0..*" Task
CompletedTaskIterator "1" --* "0..*" Task
Program --> TaskManagerFacade

@enduml

```
