﻿using System;
using System.Collections.Generic;
using System.Linq;

// Класс задачи
public class Task
{
    public string Name { get; set; }
    public bool IsCompleted { get; set; }
}

// Одиночка 
public class TaskManager
{
    private static TaskManager instance;
    private List<Task> tasks;

    private TaskManager()
    {
        tasks = new List<Task>();
    }

    public static TaskManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new TaskManager();
            }
            return instance;
        }
    }

    public void AddTask(Task task)
    {
        tasks.Add(task);
    }

    public void RemoveTask(Task task)
    {
        tasks.Remove(task);
    }

    public IEnumerable<Task> GetTasks()
    {
        return tasks;
    }

    public ITaskIterator GetUncompletedTaskIterator()
    {
        return new UncompletedTaskIterator(tasks);
    }

    public ITaskIterator GetCompletedTaskIterator()
    {
        return new CompletedTaskIterator(tasks);
    }
}

// Интерфейс итератора для задач
public interface ITaskIterator
{
    bool HasNext();
    Task GetNext();
}

// Итератор для незавершенных задач
public class UncompletedTaskIterator : ITaskIterator
{
    private List<Task> tasks;
    private int currentIndex;

    public UncompletedTaskIterator(List<Task> tasks)
    {
        this.tasks = tasks;
        currentIndex = 0;
    }

    public bool HasNext()
    {
        return currentIndex < tasks.Count;
    }

    public Task GetNext()
    {
        if (HasNext())
        {
            Task task = tasks[currentIndex];
            currentIndex++;
            return task;
        }
        return null;
    }
}

// Итератор для завершенных задач
public class CompletedTaskIterator : ITaskIterator
{
    private List<Task> tasks;
    private int currentIndex;

    public CompletedTaskIterator(List<Task> tasks)
    {
        this.tasks = tasks;
        currentIndex = 0;
    }

    public bool HasNext()
    {
        return currentIndex < tasks.Count;
    }

    public Task GetNext()
    {
        if (HasNext())
        {
            Task task = tasks[currentIndex];
            currentIndex++;
            return task;
        }
        return null;
    }
}

// Фасад 
public class TaskManagerFacade
{
    private TaskManager taskManager;
    private List<Task> uncompletedTasks;
    private List<Task> completedTasks;

    public TaskManagerFacade()
    {
        taskManager = TaskManager.Instance;
        uncompletedTasks = new List<Task>();
        completedTasks = new List<Task>();
    }

    public void AddNewTask(string taskName)
    {
        Task newTask = new Task { Name = taskName, IsCompleted = false };
        taskManager.AddTask(newTask);
        uncompletedTasks.Add(newTask);
    }

    public void CompleteTask(Task task)
    {
        task.IsCompleted = true;
        uncompletedTasks.Remove(task);
        completedTasks.Add(task);
    }

    public ITaskIterator GetUncompletedTaskIterator()
    {
        return taskManager.GetUncompletedTaskIterator();
    }

    public ITaskIterator GetCompletedTaskIterator()
    {
        return taskManager.GetCompletedTaskIterator();
    }
}

// Пример использования
public class Program
{
    public static void Main(string[] args)
    {
        TaskManagerFacade taskManagerFacade = new TaskManagerFacade();

        taskManagerFacade.AddNewTask("Task 1");
        taskManagerFacade.AddNewTask("Task 2");

        var uncompletedTaskIterator = taskManagerFacade.GetUncompletedTaskIterator();
        Console.WriteLine("Uncompleted Tasks:");
        while (uncompletedTaskIterator.HasNext())
        {
            Task task = uncompletedTaskIterator.GetNext();
            Console.WriteLine(task.Name);
        }

        var completedTaskIterator = taskManagerFacade.GetCompletedTaskIterator();
        Console.WriteLine("Completed Tasks:");
        while (completedTaskIterator.HasNext())
        {
            Task task = completedTaskIterator.GetNext();
            Console.WriteLine(task.Name);
        }
    }

}
